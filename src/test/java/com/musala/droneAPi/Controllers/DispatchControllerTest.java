package com.musala.droneAPi.Controllers;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.musala.droneAPi.Requests.LoadDroneWithMedicationRequest;
import com.musala.droneAPi.Requests.RegisterNewDroneRequest;
import com.musala.droneAPi.pojos.Drone;
import com.musala.droneAPi.pojos.Medication;
import com.musala.droneAPi.pojos.Model;
import com.musala.droneAPi.pojos.State;
import com.musala.droneAPi.services.DroneActionsService;

import java.util.ArrayList;
import java.util.Optional;

import org.apache.commons.lang3.tuple.ImmutablePair;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ContextConfiguration(classes = {DispatchController.class})
@ExtendWith(SpringExtension.class)
class DispatchControllerTest {
    @Autowired
    private DispatchController dispatchController;

    @MockBean
    private DroneActionsService droneActionsService;

    /**
     * Method under test: {@link DispatchController#loadMedicationToDrone(LoadDroneWithMedicationRequest)}
     */
    @Test
    void testLoadMedicationToDrone() throws Exception {
        Drone drone = new Drone();
        drone.setBattery_capacity(10.0d);
        drone.setCurrent_carry_weight(3);
        drone.setId(123L);
        drone.setLoaded_medications(new ArrayList<>());
        drone.setModel(Model.Lighweight);
        drone.setSerial("Serial");
        drone.setState(State.IDLE);
        drone.setWeight(3);
        Optional<Drone> ofResult = Optional.of(drone);
        when(this.droneActionsService.loadMedicationToDrone((Drone) any(), (Medication) any()))
                .thenReturn(new ImmutablePair<>(true, "Right"));
        when(this.droneActionsService.getDrone((String) any())).thenReturn(ofResult);

        Medication medication = new Medication();
        medication.setCode("Code");
        medication.setDrone_id("Drone id");
        medication.setId(123L);
        medication.setImage("Image");
        medication.setName("Name");
        medication.setWeight(3);

        LoadDroneWithMedicationRequest loadDroneWithMedicationRequest = new LoadDroneWithMedicationRequest();
        loadDroneWithMedicationRequest.setMedication(medication);
        loadDroneWithMedicationRequest.setSerial_number("42");
        String content = (new ObjectMapper()).writeValueAsString(loadDroneWithMedicationRequest);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/v1/drones/loadMedication")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.dispatchController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().isAccepted())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content()
                        .string(
                                "{\"responseCode\":200,\"status\":\"Success\",\"description\":\"Right\",\"data\":\"Operation successful\"}"));
    }

    /**
     * Method under test: {@link DispatchController#loadMedicationToDrone(LoadDroneWithMedicationRequest)}
     */
    @Test
    void testLoadMedicationToDrone2() throws Exception {
        Drone drone = new Drone();
        drone.setBattery_capacity(10.0d);
        drone.setCurrent_carry_weight(3);
        drone.setId(123L);
        drone.setLoaded_medications(new ArrayList<>());
        drone.setModel(Model.Lighweight);
        drone.setSerial("Serial");
        drone.setState(State.IDLE);
        drone.setWeight(3);
        Optional<Drone> ofResult = Optional.of(drone);
        when(this.droneActionsService.loadMedicationToDrone((Drone) any(), (Medication) any()))
                .thenReturn(new ImmutablePair<>(false, "Right"));
        when(this.droneActionsService.getDrone((String) any())).thenReturn(ofResult);

        Medication medication = new Medication();
        medication.setCode("Code");
        medication.setDrone_id("Drone id");
        medication.setId(123L);
        medication.setImage("Image");
        medication.setName("Name");
        medication.setWeight(3);

        LoadDroneWithMedicationRequest loadDroneWithMedicationRequest = new LoadDroneWithMedicationRequest();
        loadDroneWithMedicationRequest.setMedication(medication);
        loadDroneWithMedicationRequest.setSerial_number("42");
        String content = (new ObjectMapper()).writeValueAsString(loadDroneWithMedicationRequest);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/v1/drones/loadMedication")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.dispatchController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(400))
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content()
                        .string(
                                "{\"responseCode\":400,\"status\":\"Failed\",\"description\":\"Right\",\"data\":\"Operation Failed\"}"));
    }

    /**
     * Method under test: {@link DispatchController#loadMedicationToDrone(LoadDroneWithMedicationRequest)}
     */
    @Test
    void testLoadMedicationToDrone3() throws Exception {
        when(this.droneActionsService.loadMedicationToDrone((Drone) any(), (Medication) any()))
                .thenReturn(new ImmutablePair<>(true, "Right"));
        when(this.droneActionsService.getDrone((String) any())).thenReturn(Optional.empty());

        Medication medication = new Medication();
        medication.setCode("Code");
        medication.setDrone_id("Drone id");
        medication.setId(123L);
        medication.setImage("Image");
        medication.setName("Name");
        medication.setWeight(3);

        LoadDroneWithMedicationRequest loadDroneWithMedicationRequest = new LoadDroneWithMedicationRequest();
        loadDroneWithMedicationRequest.setMedication(medication);
        loadDroneWithMedicationRequest.setSerial_number("42");
        String content = (new ObjectMapper()).writeValueAsString(loadDroneWithMedicationRequest);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/v1/drones/loadMedication")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.dispatchController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(400))
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content()
                        .string(
                                "{\"responseCode\":400,\"status\":\"Failed\",\"description\":\"Please provide a valid drone serial Number\",\"data"
                                        + "\":\"Operation Failed\"}"));
    }

    /**
     * Method under test: {@link DispatchController#getDroneBattreyLevel(String)}
     */
    @Test
    void testGetDroneBattreyLevel() throws Exception {
        Drone drone = new Drone();
        drone.setBattery_capacity(10.0d);
        drone.setCurrent_carry_weight(3);
        drone.setId(123L);
        drone.setLoaded_medications(new ArrayList<>());
        drone.setModel(Model.Lighweight);
        drone.setSerial("Serial");
        drone.setState(State.IDLE);
        drone.setWeight(3);
        Optional<Drone> ofResult = Optional.of(drone);
        when(this.droneActionsService.getDrone((String) any())).thenReturn(ofResult);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/api/v1/drones/find/battreyLevel/{serial_number}", "42");
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.dispatchController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().isAccepted())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content()
                        .string(
                                "{\"responseCode\":200,\"status\":\"Success\",\"description\":\"Operation successful!!\",\"data\":{\"Battery"
                                        + " Level\":10.0,\"Serial Number\":\"Serial\"}}"));
    }

    /**
     * Method under test: {@link DispatchController#listAllDrones()}
     */
    @Test
    void testListAllDrones() throws Exception {
        when(this.droneActionsService.listAllDrones()).thenReturn(Optional.of(new ArrayList<>()));
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/v1/drones/listAll");
        MockMvcBuilders.standaloneSetup(this.dispatchController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content().string("[]"));
    }

    /**
     * Method under test: {@link DispatchController#listAllDrones()}
     */
    @Test
    void testListAllDrones2() throws Exception {
        when(this.droneActionsService.listAllDrones()).thenReturn(Optional.of(new ArrayList<>()));
        MockHttpServletRequestBuilder getResult = MockMvcRequestBuilders.get("/api/v1/drones/listAll");
        getResult.contentType("https://example.org/example");
        MockMvcBuilders.standaloneSetup(this.dispatchController)
                .build()
                .perform(getResult)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content().string("[]"));
    }

    /**
     * Method under test: {@link DispatchController#getDroneBattreyLevel(String)}
     */
    @Test
    void testGetDroneBattreyLevel2() throws Exception {
        when(this.droneActionsService.getDrone((String) any())).thenReturn(Optional.empty());
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/api/v1/drones/find/battreyLevel/{serial_number}", "42");
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.dispatchController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(400))
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content()
                        .string(
                                "{\"responseCode\":500,\"status\":\"Failed\",\"description\":\"Please provided a valid Drone Serial Number!\","
                                        + "\"data\":{}}"));
    }

    /**
     * Method under test: {@link DispatchController#getDroneBattreyLevel(String)}
     */
    @Test
    void testGetDroneBattreyLevel3() throws Exception {
        Drone drone = new Drone();
        drone.setBattery_capacity(10.0d);
        drone.setCurrent_carry_weight(3);
        drone.setId(123L);
        drone.setLoaded_medications(new ArrayList<>());
        drone.setModel(Model.Lighweight);
        drone.setSerial("Serial");
        drone.setState(State.IDLE);
        drone.setWeight(3);
        Optional<Drone> ofResult = Optional.of(drone);
        when(this.droneActionsService.getDrone((String) any())).thenReturn(ofResult);
        MockHttpServletRequestBuilder getResult = MockMvcRequestBuilders
                .get("/api/v1/drones/find/battreyLevel/{serial_number}", "42");
        getResult.contentType("https://example.org/example");
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.dispatchController)
                .build()
                .perform(getResult);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().isAccepted())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content()
                        .string(
                                "{\"responseCode\":200,\"status\":\"Success\",\"description\":\"Operation successful!!\",\"data\":{\"Battery"
                                        + " Level\":10.0,\"Serial Number\":\"Serial\"}}"));
    }

    /**
     * Method under test: {@link DispatchController#getDroneBySerialNumber(String)}
     */
    @Test
    void testGetDroneBySerialNumber() throws Exception {
        Drone drone = new Drone();
        drone.setBattery_capacity(10.0d);
        drone.setCurrent_carry_weight(3);
        drone.setId(123L);
        drone.setLoaded_medications(new ArrayList<>());
        drone.setModel(Model.Lighweight);
        drone.setSerial("Serial");
        drone.setState(State.IDLE);
        drone.setWeight(3);
        Optional<Drone> ofResult = Optional.of(drone);
        when(this.droneActionsService.getDrone((String) any())).thenReturn(ofResult);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/v1/drones/find/{serial_number}",
                "42");
        MockMvcBuilders.standaloneSetup(this.dispatchController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content()
                        .string(
                                "{\"id\":123,\"serial\":\"Serial\",\"model\":\"Lighweight\",\"battery_capacity\":10.0,\"weight\":3,\"current_carry"
                                        + "_weight\":3,\"state\":\"IDLE\",\"loaded_medications\":[]}"));
    }

    /**
     * Method under test: {@link DispatchController#getDroneBySerialNumber(String)}
     */
    @Test
    void testGetDroneBySerialNumber2() throws Exception {
        Drone drone = new Drone();
        drone.setBattery_capacity(10.0d);
        drone.setCurrent_carry_weight(3);
        drone.setId(123L);
        drone.setLoaded_medications(new ArrayList<>());
        drone.setModel(Model.Lighweight);
        drone.setSerial("Serial");
        drone.setState(State.IDLE);
        drone.setWeight(3);
        Optional<Drone> ofResult = Optional.of(drone);
        when(this.droneActionsService.getDrone((String) any())).thenReturn(ofResult);
        MockHttpServletRequestBuilder getResult = MockMvcRequestBuilders.get("/api/v1/drones/find/{serial_number}", "42");
        getResult.contentType("https://example.org/example");
        MockMvcBuilders.standaloneSetup(this.dispatchController)
                .build()
                .perform(getResult)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content()
                        .string(
                                "{\"id\":123,\"serial\":\"Serial\",\"model\":\"Lighweight\",\"battery_capacity\":10.0,\"weight\":3,\"current_carry"
                                        + "_weight\":3,\"state\":\"IDLE\",\"loaded_medications\":[]}"));
    }

    /**
     * Method under test: {@link DispatchController#listAllDronesAvailableForLoading()}
     */
    @Test
    void testListAllDronesAvailableForLoading() throws Exception {
        when(this.droneActionsService.getAvalableDronesForLoading()).thenReturn(Optional.of(new ArrayList<>()));
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/api/v1/drones/list/availableForLoading");
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.dispatchController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().isAccepted())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content()
                        .string(
                                "{\"responseCode\":200,\"status\":\"Success\",\"description\":\"Operation successful!!\",\"data\":[]}"));
    }

    /**
     * Method under test: {@link DispatchController#listAllDronesAvailableForLoading()}
     */
    @Test
    void testListAllDronesAvailableForLoading2() throws Exception {
        when(this.droneActionsService.getAvalableDronesForLoading()).thenReturn(Optional.of(new ArrayList<>()));
        MockHttpServletRequestBuilder getResult = MockMvcRequestBuilders.get("/api/v1/drones/list/availableForLoading");
        getResult.contentType("https://example.org/example");
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.dispatchController)
                .build()
                .perform(getResult);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().isAccepted())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content()
                        .string(
                                "{\"responseCode\":200,\"status\":\"Success\",\"description\":\"Operation successful!!\",\"data\":[]}"));
    }

    /**
     * Method under test: {@link DispatchController#registerNewDrones(RegisterNewDroneRequest)}
     */
    @Test
    void testRegisterNewDrones() throws Exception {
        when(this.droneActionsService.saveNewDrone((com.musala.droneAPi.pojos.Drone) any()))
                .thenReturn(new ImmutablePair<>(true, "Right"));

        RegisterNewDroneRequest registerNewDroneRequest = new RegisterNewDroneRequest();
        registerNewDroneRequest.setBattery_capacity(10.0d);
        registerNewDroneRequest.setModel(Model.Lighweight);
        registerNewDroneRequest.setSerial("Serial");
        registerNewDroneRequest.setState(State.IDLE);
        registerNewDroneRequest.setWeight(3);
        String content = (new ObjectMapper()).writeValueAsString(registerNewDroneRequest);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/v1/drones/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.dispatchController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().isAccepted())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content()
                        .string(
                                "{\"responseCode\":200,\"status\":\"Success\",\"description\":\"Right\",\"data\":\"Operation successful\"}"));
    }

    /**
     * Method under test: {@link DispatchController#registerNewDrones(RegisterNewDroneRequest)}
     */
    @Test
    void testRegisterNewDrones2() throws Exception {
        when(this.droneActionsService.saveNewDrone((com.musala.droneAPi.pojos.Drone) any()))
                .thenReturn(new ImmutablePair<>(false, "Right"));

        RegisterNewDroneRequest registerNewDroneRequest = new RegisterNewDroneRequest();
        registerNewDroneRequest.setBattery_capacity(10.0d);
        registerNewDroneRequest.setModel(Model.Lighweight);
        registerNewDroneRequest.setSerial("Serial");
        registerNewDroneRequest.setState(State.IDLE);
        registerNewDroneRequest.setWeight(3);
        String content = (new ObjectMapper()).writeValueAsString(registerNewDroneRequest);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/v1/drones/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.dispatchController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(400))
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content()
                        .string(
                                "{\"responseCode\":400,\"status\":\"Failed\",\"description\":\"Right\",\"data\":\"Operation Failed\"}"));
    }
}

