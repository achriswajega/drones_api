package com.musala.droneAPi.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.musala.droneAPi.dao.DroneRepository;
import com.musala.droneAPi.pojos.Drone;
import com.musala.droneAPi.pojos.Medication;
import com.musala.droneAPi.pojos.Model;
import com.musala.droneAPi.pojos.State;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {DroneActionsService.class})
@ExtendWith(SpringExtension.class)
class DroneActionsServiceTest {
    @Autowired
    private DroneActionsService droneActionsService;

    @MockBean
    private DroneRepository droneRepository;

    /**
     * Method under test: {@link DroneActionsService#saveNewDrone(Drone)}
     */
    @Test
    void testSaveNewDrone() {
        Drone drone = new Drone();
        drone.setBattery_capacity(10.0d);
        drone.setCurrent_carry_weight(3);
        drone.setId(123L);
        drone.setLoaded_medications(new ArrayList<>());
        drone.setModel(Model.Lighweight);
        drone.setSerial("Serial");
        drone.setState(State.IDLE);
        drone.setWeight(3);
        Pair<Boolean, String> actualSaveNewDroneResult = this.droneActionsService.saveNewDrone(drone);
        assertFalse(actualSaveNewDroneResult.getKey());
        assertEquals("The provided drone battery capacity should be between 0 and 1", actualSaveNewDroneResult.getValue());
    }

    /**
     * Method under test: {@link DroneActionsService#saveNewDrone(Drone)}
     */
    @Test
    void testSaveNewDrone2() {
        Drone drone = new Drone();
        drone.setBattery_capacity(10.0d);
        drone.setCurrent_carry_weight(3);
        drone.setId(123L);
        drone.setLoaded_medications(new ArrayList<>());
        drone.setModel(Model.Lighweight);
        drone.setSerial("Serial");
        drone.setState(State.IDLE);
        drone.setWeight(3);
        when(this.droneRepository.save((Drone) any())).thenReturn(drone);
        Drone drone1 = mock(Drone.class);
        when(drone1.getModel()).thenReturn(Model.Lighweight);
        when(drone1.getBattery_capacity()).thenReturn(0.5d);
        when(drone1.getWeight()).thenReturn(3);
        when(drone1.getSerial()).thenReturn("Serial");
        doNothing().when(drone1).setBattery_capacity((Double) any());
        doNothing().when(drone1).setCurrent_carry_weight((Integer) any());
        doNothing().when(drone1).setId((Long) any());
        doNothing().when(drone1).setLoaded_medications((java.util.List<Medication>) any());
        doNothing().when(drone1).setModel((Model) any());
        doNothing().when(drone1).setSerial((String) any());
        doNothing().when(drone1).setState((State) any());
        doNothing().when(drone1).setWeight((Integer) any());
        drone1.setBattery_capacity(10.0d);
        drone1.setCurrent_carry_weight(3);
        drone1.setId(123L);
        drone1.setLoaded_medications(new ArrayList<>());
        drone1.setModel(Model.Lighweight);
        drone1.setSerial("Serial");
        drone1.setState(State.IDLE);
        drone1.setWeight(3);
        Pair<Boolean, String> actualSaveNewDroneResult = this.droneActionsService.saveNewDrone(drone1);
        assertTrue(actualSaveNewDroneResult.getKey());
        assertEquals("Operation successful", actualSaveNewDroneResult.getValue());
        verify(this.droneRepository).save((Drone) any());
        verify(drone1, atLeast(1)).getModel();
        verify(drone1, atLeast(1)).getBattery_capacity();
        verify(drone1).getWeight();
        verify(drone1, atLeast(1)).getSerial();
        verify(drone1).setBattery_capacity((Double) any());
        verify(drone1).setCurrent_carry_weight((Integer) any());
        verify(drone1).setId((Long) any());
        verify(drone1).setLoaded_medications((java.util.List<Medication>) any());
        verify(drone1).setModel((Model) any());
        verify(drone1).setSerial((String) any());
        verify(drone1).setState((State) any());
        verify(drone1).setWeight((Integer) any());
    }

    /**
     * Method under test: {@link DroneActionsService#saveNewDrone(Drone)}
     */
    @Test
    void testSaveNewDrone3() {
        Drone drone = new Drone();
        drone.setBattery_capacity(10.0d);
        drone.setCurrent_carry_weight(3);
        drone.setId(123L);
        drone.setLoaded_medications(new ArrayList<>());
        drone.setModel(Model.Lighweight);
        drone.setSerial("Serial");
        drone.setState(State.IDLE);
        drone.setWeight(3);
        when(this.droneRepository.save((Drone) any())).thenReturn(drone);
        Drone drone1 = mock(Drone.class);
        when(drone1.getModel()).thenReturn(Model.Lighweight);
        when(drone1.getBattery_capacity()).thenReturn(-0.5d);
        when(drone1.getWeight()).thenReturn(3);
        when(drone1.getSerial()).thenReturn("Serial");
        doNothing().when(drone1).setBattery_capacity((Double) any());
        doNothing().when(drone1).setCurrent_carry_weight((Integer) any());
        doNothing().when(drone1).setId((Long) any());
        doNothing().when(drone1).setLoaded_medications((java.util.List<Medication>) any());
        doNothing().when(drone1).setModel((Model) any());
        doNothing().when(drone1).setSerial((String) any());
        doNothing().when(drone1).setState((State) any());
        doNothing().when(drone1).setWeight((Integer) any());
        drone1.setBattery_capacity(10.0d);
        drone1.setCurrent_carry_weight(3);
        drone1.setId(123L);
        drone1.setLoaded_medications(new ArrayList<>());
        drone1.setModel(Model.Lighweight);
        drone1.setSerial("Serial");
        drone1.setState(State.IDLE);
        drone1.setWeight(3);
        Pair<Boolean, String> actualSaveNewDroneResult = this.droneActionsService.saveNewDrone(drone1);
        assertFalse(actualSaveNewDroneResult.getKey());
        assertEquals("The provided drone battery capacity should be between 0 and 1", actualSaveNewDroneResult.getValue());
        verify(drone1, atLeast(1)).getModel();
        verify(drone1, atLeast(1)).getBattery_capacity();
        verify(drone1).getWeight();
        verify(drone1, atLeast(1)).getSerial();
        verify(drone1).setBattery_capacity((Double) any());
        verify(drone1).setCurrent_carry_weight((Integer) any());
        verify(drone1).setId((Long) any());
        verify(drone1).setLoaded_medications((java.util.List<Medication>) any());
        verify(drone1).setModel((Model) any());
        verify(drone1).setSerial((String) any());
        verify(drone1).setState((State) any());
        verify(drone1).setWeight((Integer) any());
    }

    /**
     * Method under test: {@link DroneActionsService#saveNewDrone(Drone)}
     */
    @Test
    void testSaveNewDrone4() {
        Drone drone = new Drone();
        drone.setBattery_capacity(10.0d);
        drone.setCurrent_carry_weight(3);
        drone.setId(123L);
        drone.setLoaded_medications(new ArrayList<>());
        drone.setModel(Model.Lighweight);
        drone.setSerial("Serial");
        drone.setState(State.IDLE);
        drone.setWeight(3);
        when(this.droneRepository.save((Drone) any())).thenReturn(drone);
        Drone drone1 = mock(Drone.class);
        when(drone1.getModel()).thenReturn(Model.Lighweight);
        when(drone1.getBattery_capacity()).thenReturn(0.5d);
        when(drone1.getWeight()).thenReturn(null);
        when(drone1.getSerial()).thenReturn("Serial");
        doNothing().when(drone1).setBattery_capacity((Double) any());
        doNothing().when(drone1).setCurrent_carry_weight((Integer) any());
        doNothing().when(drone1).setId((Long) any());
        doNothing().when(drone1).setLoaded_medications((java.util.List<Medication>) any());
        doNothing().when(drone1).setModel((Model) any());
        doNothing().when(drone1).setSerial((String) any());
        doNothing().when(drone1).setState((State) any());
        doNothing().when(drone1).setWeight((Integer) any());
        drone1.setBattery_capacity(10.0d);
        drone1.setCurrent_carry_weight(3);
        drone1.setId(123L);
        drone1.setLoaded_medications(new ArrayList<>());
        drone1.setModel(Model.Lighweight);
        drone1.setSerial("Serial");
        drone1.setState(State.IDLE);
        drone1.setWeight(3);
        Pair<Boolean, String> actualSaveNewDroneResult = this.droneActionsService.saveNewDrone(drone1);
        assertFalse(actualSaveNewDroneResult.getKey());
        assertEquals("An internal error occurred", actualSaveNewDroneResult.getValue());
        verify(drone1, atLeast(1)).getModel();
        verify(drone1).getWeight();
        verify(drone1, atLeast(1)).getSerial();
        verify(drone1).setBattery_capacity((Double) any());
        verify(drone1).setCurrent_carry_weight((Integer) any());
        verify(drone1).setId((Long) any());
        verify(drone1).setLoaded_medications((java.util.List<Medication>) any());
        verify(drone1).setModel((Model) any());
        verify(drone1).setSerial((String) any());
        verify(drone1).setState((State) any());
        verify(drone1).setWeight((Integer) any());
    }

    /**
     * Method under test: {@link DroneActionsService#loadMedicationToDrone(Drone, Medication)}
     */
    @Test
    void testLoadMedicationToDrone() {
        Drone drone = new Drone();
        drone.setBattery_capacity(10.0d);
        drone.setCurrent_carry_weight(3);
        drone.setId(123L);
        drone.setLoaded_medications(new ArrayList<>());
        drone.setModel(Model.Lighweight);
        drone.setSerial("Serial");
        drone.setState(State.IDLE);
        drone.setWeight(3);

        Medication medication = new Medication();
        medication.setCode("Code");
        medication.setDrone_id("Drone id");
        medication.setId(123L);
        medication.setImage("Image");
        medication.setName("Name");
        medication.setWeight(3);
        Pair<Boolean, String> actualLoadMedicationToDroneResult = this.droneActionsService.loadMedicationToDrone(drone,
                medication);
        assertFalse(actualLoadMedicationToDroneResult.getKey());
        assertEquals("Code only allows upper class letters,numbers,'-',''_", actualLoadMedicationToDroneResult.getValue());
    }

    /**
     * Method under test: {@link DroneActionsService#loadMedicationToDrone(Drone, Medication)}
     */
    @Test
    void testLoadMedicationToDrone2() {
        Drone drone = new Drone();
        drone.setBattery_capacity(10.0d);
        drone.setCurrent_carry_weight(3);
        drone.setId(123L);
        drone.setLoaded_medications(new ArrayList<>());
        drone.setModel(Model.Lighweight);
        drone.setSerial("Serial");
        drone.setState(State.IDLE);
        drone.setWeight(3);
        when(this.droneRepository.saveAndFlush((Drone) any())).thenReturn(drone);

        Drone drone1 = new Drone();
        drone1.setBattery_capacity(10.0d);
        drone1.setCurrent_carry_weight(3);
        drone1.setId(123L);
        drone1.setLoaded_medications(new ArrayList<>());
        drone1.setModel(Model.Lighweight);
        drone1.setSerial("Serial");
        drone1.setState(State.IDLE);
        drone1.setWeight(3);
        Medication medication = mock(Medication.class);
        when(medication.getWeight()).thenReturn(3);
        when(medication.getCode()).thenReturn("com.musala.droneAPi.pojos.Medication");
        when(medication.getName()).thenReturn("Name");
        doNothing().when(medication).setCode((String) any());
        doNothing().when(medication).setDrone_id((String) any());
        doNothing().when(medication).setId((Long) any());
        doNothing().when(medication).setImage((String) any());
        doNothing().when(medication).setName((String) any());
        doNothing().when(medication).setWeight((Integer) any());
        medication.setCode("Code");
        medication.setDrone_id("Drone id");
        medication.setId(123L);
        medication.setImage("Image");
        medication.setName("Name");
        medication.setWeight(3);
        Pair<Boolean, String> actualLoadMedicationToDroneResult = this.droneActionsService.loadMedicationToDrone(drone1,
                medication);
        assertTrue(actualLoadMedicationToDroneResult.getKey());
        assertEquals("Operation successful", actualLoadMedicationToDroneResult.getValue());
        verify(this.droneRepository).saveAndFlush((Drone) any());
        verify(medication, atLeast(1)).getWeight();
        verify(medication).getCode();
        verify(medication, atLeast(1)).getName();
        verify(medication).setCode((String) any());
        verify(medication).setDrone_id((String) any());
        verify(medication).setId((Long) any());
        verify(medication).setImage((String) any());
        verify(medication).setName((String) any());
        verify(medication).setWeight((Integer) any());
        assertEquals(1, drone1.getLoaded_medications().size());
        assertEquals(6, drone1.getCurrent_carry_weight().intValue());
    }

    /**
     * Method under test: {@link DroneActionsService#loadMedicationToDrone(Drone, Medication)}
     */
    @Test
    void testLoadMedicationToDrone3() {
        Drone drone = new Drone();
        drone.setBattery_capacity(10.0d);
        drone.setCurrent_carry_weight(3);
        drone.setId(123L);
        drone.setLoaded_medications(new ArrayList<>());
        drone.setModel(Model.Lighweight);
        drone.setSerial("Serial");
        drone.setState(State.IDLE);
        drone.setWeight(3);
        when(this.droneRepository.saveAndFlush((Drone) any())).thenReturn(drone);
        Drone drone1 = mock(Drone.class);
        when(drone1.getModel()).thenReturn(Model.Lighweight);
        when(drone1.getBattery_capacity()).thenReturn(-0.5d);
        when(drone1.getSerial()).thenReturn("Serial");
        when(drone1.getLoaded_medications()).thenReturn(new ArrayList<>());
        doNothing().when(drone1).setBattery_capacity((Double) any());
        doNothing().when(drone1).setCurrent_carry_weight((Integer) any());
        doNothing().when(drone1).setId((Long) any());
        doNothing().when(drone1).setLoaded_medications((List<Medication>) any());
        doNothing().when(drone1).setModel((Model) any());
        doNothing().when(drone1).setSerial((String) any());
        doNothing().when(drone1).setState((State) any());
        doNothing().when(drone1).setWeight((Integer) any());
        drone1.setBattery_capacity(10.0d);
        drone1.setCurrent_carry_weight(3);
        drone1.setId(123L);
        drone1.setLoaded_medications(new ArrayList<>());
        drone1.setModel(Model.Lighweight);
        drone1.setSerial("Serial");
        drone1.setState(State.IDLE);
        drone1.setWeight(3);
        Medication medication = mock(Medication.class);
        when(medication.getWeight()).thenReturn(3);
        when(medication.getCode()).thenReturn("com.musala.droneAPi.pojos.Medication");
        when(medication.getName()).thenReturn("Name");
        doNothing().when(medication).setCode((String) any());
        doNothing().when(medication).setDrone_id((String) any());
        doNothing().when(medication).setId((Long) any());
        doNothing().when(medication).setImage((String) any());
        doNothing().when(medication).setName((String) any());
        doNothing().when(medication).setWeight((Integer) any());
        medication.setCode("Code");
        medication.setDrone_id("Drone id");
        medication.setId(123L);
        medication.setImage("Image");
        medication.setName("Name");
        medication.setWeight(3);
        Pair<Boolean, String> actualLoadMedicationToDroneResult = this.droneActionsService.loadMedicationToDrone(drone1,
                medication);
        assertFalse(actualLoadMedicationToDroneResult.getKey());
        assertEquals("Drone battery Capacity is low!", actualLoadMedicationToDroneResult.getValue());
        verify(drone1, atLeast(1)).getModel();
        verify(drone1).getBattery_capacity();
        verify(drone1, atLeast(1)).getSerial();
        verify(drone1).getLoaded_medications();
        verify(drone1).setBattery_capacity((Double) any());
        verify(drone1).setCurrent_carry_weight((Integer) any());
        verify(drone1).setId((Long) any());
        verify(drone1).setLoaded_medications((List<Medication>) any());
        verify(drone1).setModel((Model) any());
        verify(drone1).setSerial((String) any());
        verify(drone1).setState((State) any());
        verify(drone1).setWeight((Integer) any());
        verify(medication).getWeight();
        verify(medication).getCode();
        verify(medication, atLeast(1)).getName();
        verify(medication).setCode((String) any());
        verify(medication).setDrone_id((String) any());
        verify(medication).setId((Long) any());
        verify(medication).setImage((String) any());
        verify(medication).setName((String) any());
        verify(medication).setWeight((Integer) any());
    }

    /**
     * Method under test: {@link DroneActionsService#getLoadedMedicationsOnDrone(Drone)}
     */
    @Test
    void testGetLoadedMedicationsOnDrone() {
        Drone drone = new Drone();
        drone.setBattery_capacity(10.0d);
        drone.setCurrent_carry_weight(3);
        drone.setId(123L);
        drone.setLoaded_medications(new ArrayList<>());
        drone.setModel(Model.Lighweight);
        drone.setSerial("Serial");
        drone.setState(State.IDLE);
        drone.setWeight(3);
        Triple<Boolean, Optional<List<Medication>>, String> actualLoadedMedicationsOnDrone = this.droneActionsService
                .getLoadedMedicationsOnDrone(drone);
        assertFalse(actualLoadedMedicationsOnDrone.getLeft());
        assertEquals("The provided drone battery capacity should be between 0 and 1",
                actualLoadedMedicationsOnDrone.getRight());
        assertNull(actualLoadedMedicationsOnDrone.getMiddle());
    }

    /**
     * Method under test: {@link DroneActionsService#getLoadedMedicationsOnDrone(Drone)}
     */
    @Test
    void testGetLoadedMedicationsOnDrone2() {
        Drone drone = mock(Drone.class);
        when(drone.getModel()).thenReturn(Model.Lighweight);
        when(drone.getBattery_capacity()).thenReturn(0.5d);
        when(drone.getWeight()).thenReturn(3);
        when(drone.getSerial()).thenReturn("Serial");
        when(drone.getLoaded_medications()).thenReturn(new ArrayList<>());
        doNothing().when(drone).setBattery_capacity((Double) any());
        doNothing().when(drone).setCurrent_carry_weight((Integer) any());
        doNothing().when(drone).setId((Long) any());
        doNothing().when(drone).setLoaded_medications((List<Medication>) any());
        doNothing().when(drone).setModel((Model) any());
        doNothing().when(drone).setSerial((String) any());
        doNothing().when(drone).setState((State) any());
        doNothing().when(drone).setWeight((Integer) any());
        drone.setBattery_capacity(10.0d);
        drone.setCurrent_carry_weight(3);
        drone.setId(123L);
        drone.setLoaded_medications(new ArrayList<>());
        drone.setModel(Model.Lighweight);
        drone.setSerial("Serial");
        drone.setState(State.IDLE);
        drone.setWeight(3);
        Triple<Boolean, Optional<List<Medication>>, String> actualLoadedMedicationsOnDrone = this.droneActionsService
                .getLoadedMedicationsOnDrone(drone);
        assertFalse(actualLoadedMedicationsOnDrone.getLeft());
        assertEquals("Operation successful", actualLoadedMedicationsOnDrone.getRight());
        assertTrue(actualLoadedMedicationsOnDrone.getMiddle().isPresent());
        verify(drone).getModel();
        verify(drone, atLeast(1)).getBattery_capacity();
        verify(drone).getWeight();
        verify(drone).getSerial();
        verify(drone).getLoaded_medications();
        verify(drone).setBattery_capacity((Double) any());
        verify(drone).setCurrent_carry_weight((Integer) any());
        verify(drone).setId((Long) any());
        verify(drone).setLoaded_medications((List<Medication>) any());
        verify(drone).setModel((Model) any());
        verify(drone).setSerial((String) any());
        verify(drone).setState((State) any());
        verify(drone).setWeight((Integer) any());
    }

    /**
     * Method under test: {@link DroneActionsService#getLoadedMedicationsOnDrone(Drone)}
     */
    @Test
    void testGetLoadedMedicationsOnDrone3() {
        Drone drone = mock(Drone.class);
        when(drone.getModel()).thenReturn(Model.Lighweight);
        when(drone.getBattery_capacity()).thenReturn(-0.5d);
        when(drone.getWeight()).thenReturn(3);
        when(drone.getSerial()).thenReturn("Serial");
        when(drone.getLoaded_medications()).thenReturn(new ArrayList<>());
        doNothing().when(drone).setBattery_capacity((Double) any());
        doNothing().when(drone).setCurrent_carry_weight((Integer) any());
        doNothing().when(drone).setId((Long) any());
        doNothing().when(drone).setLoaded_medications((List<Medication>) any());
        doNothing().when(drone).setModel((Model) any());
        doNothing().when(drone).setSerial((String) any());
        doNothing().when(drone).setState((State) any());
        doNothing().when(drone).setWeight((Integer) any());
        drone.setBattery_capacity(10.0d);
        drone.setCurrent_carry_weight(3);
        drone.setId(123L);
        drone.setLoaded_medications(new ArrayList<>());
        drone.setModel(Model.Lighweight);
        drone.setSerial("Serial");
        drone.setState(State.IDLE);
        drone.setWeight(3);
        Triple<Boolean, Optional<List<Medication>>, String> actualLoadedMedicationsOnDrone = this.droneActionsService
                .getLoadedMedicationsOnDrone(drone);
        assertFalse(actualLoadedMedicationsOnDrone.getLeft());
        assertEquals("The provided drone battery capacity should be between 0 and 1",
                actualLoadedMedicationsOnDrone.getRight());
        assertNull(actualLoadedMedicationsOnDrone.getMiddle());
        verify(drone, atLeast(1)).getModel();
        verify(drone, atLeast(1)).getBattery_capacity();
        verify(drone).getWeight();
        verify(drone, atLeast(1)).getSerial();
        verify(drone).setBattery_capacity((Double) any());
        verify(drone).setCurrent_carry_weight((Integer) any());
        verify(drone).setId((Long) any());
        verify(drone).setLoaded_medications((List<Medication>) any());
        verify(drone).setModel((Model) any());
        verify(drone).setSerial((String) any());
        verify(drone).setState((State) any());
        verify(drone).setWeight((Integer) any());
    }


    /**
     * Method under test: {@link DroneActionsService#getAvalableDronesForLoading()}
     */
    @Test
    void testGetAvalableDronesForLoading() {
        when(this.droneRepository.findAll()).thenReturn(new ArrayList<>());
        assertTrue(this.droneActionsService.getAvalableDronesForLoading().isPresent());
        verify(this.droneRepository).findAll();
    }

    /**
     * Method under test: {@link DroneActionsService#getAvalableDronesForLoading()}
     */
    @Test
    void testGetAvalableDronesForLoading2() {
        Drone drone = new Drone();
        drone.setBattery_capacity(10.0d);
        drone.setCurrent_carry_weight(3);
        drone.setId(123L);
        drone.setLoaded_medications(new ArrayList<>());
        drone.setModel(Model.Lighweight);
        drone.setSerial("Serial");
        drone.setState(State.IDLE);
        drone.setWeight(3);

        ArrayList<Drone> droneList = new ArrayList<>();
        droneList.add(drone);
        when(this.droneRepository.findAll()).thenReturn(droneList);
        assertTrue(this.droneActionsService.getAvalableDronesForLoading().isPresent());
        verify(this.droneRepository).findAll();
    }

    /**
     * Method under test: {@link DroneActionsService#getAvalableDronesForLoading()}
     */
    @Test
    void testGetAvalableDronesForLoading3() {
        Drone drone = new Drone();
        drone.setBattery_capacity(10.0d);
        drone.setCurrent_carry_weight(3);
        drone.setId(123L);
        drone.setLoaded_medications(new ArrayList<>());
        drone.setModel(Model.Lighweight);
        drone.setSerial("Serial");
        drone.setState(State.IDLE);
        drone.setWeight(3);

        Drone drone1 = new Drone();
        drone1.setBattery_capacity(0.25d);
        drone1.setCurrent_carry_weight(3);
        drone1.setId(123L);
        drone1.setLoaded_medications(new ArrayList<>());
        drone1.setModel(Model.Lighweight);
        drone1.setSerial("Serial");
        drone1.setState(State.IDLE);
        drone1.setWeight(3);

        ArrayList<Drone> droneList = new ArrayList<>();
        droneList.add(drone1);
        droneList.add(drone);
        when(this.droneRepository.findAll()).thenReturn(droneList);
        assertTrue(this.droneActionsService.getAvalableDronesForLoading().isPresent());
        verify(this.droneRepository).findAll();
    }

    /**
     * Method under test: {@link DroneActionsService#getAvalableDronesForLoading()}
     */
    @Test
    void testGetAvalableDronesForLoading4() {
        Drone drone = new Drone();
        drone.setBattery_capacity(10.0d);
        drone.setCurrent_carry_weight(500);
        drone.setId(123L);
        drone.setLoaded_medications(new ArrayList<>());
        drone.setModel(Model.Lighweight);
        drone.setSerial("Serial");
        drone.setState(State.IDLE);
        drone.setWeight(3);

        Drone drone1 = new Drone();
        drone1.setBattery_capacity(0.25d);
        drone1.setCurrent_carry_weight(3);
        drone1.setId(123L);
        drone1.setLoaded_medications(new ArrayList<>());
        drone1.setModel(Model.Lighweight);
        drone1.setSerial("Serial");
        drone1.setState(State.IDLE);
        drone1.setWeight(3);

        ArrayList<Drone> droneList = new ArrayList<>();
        droneList.add(drone1);
        droneList.add(drone);
        when(this.droneRepository.findAll()).thenReturn(droneList);
        assertTrue(this.droneActionsService.getAvalableDronesForLoading().isPresent());
        verify(this.droneRepository).findAll();
    }

    /**
     * Method under test: {@link DroneActionsService#checkDroneBatteryLevel(Drone)}
     */
    @Test
    void testCheckDroneBatteryLevel() {
        Drone drone = new Drone();
        drone.setBattery_capacity(10.0d);
        drone.setCurrent_carry_weight(3);
        drone.setId(123L);
        drone.setLoaded_medications(new ArrayList<>());
        drone.setModel(Model.Lighweight);
        drone.setSerial("Serial");
        drone.setState(State.IDLE);
        drone.setWeight(3);
        Optional<Double> actualCheckDroneBatteryLevelResult = this.droneActionsService.checkDroneBatteryLevel(drone);
        assertTrue(actualCheckDroneBatteryLevelResult.isPresent());
        assertEquals(10.0d, actualCheckDroneBatteryLevelResult.get());
    }

    /**
     * Method under test: {@link DroneActionsService#checkDroneBatteryLevel(Drone)}
     */
    @Test
    void testCheckDroneBatteryLevel2() {
        Drone drone = mock(Drone.class);
        when(drone.getBattery_capacity()).thenReturn(10.0d);
        doNothing().when(drone).setBattery_capacity((Double) any());
        doNothing().when(drone).setCurrent_carry_weight((Integer) any());
        doNothing().when(drone).setId((Long) any());
        doNothing().when(drone).setLoaded_medications((java.util.List<Medication>) any());
        doNothing().when(drone).setModel((Model) any());
        doNothing().when(drone).setSerial((String) any());
        doNothing().when(drone).setState((State) any());
        doNothing().when(drone).setWeight((Integer) any());
        drone.setBattery_capacity(10.0d);
        drone.setCurrent_carry_weight(3);
        drone.setId(123L);
        drone.setLoaded_medications(new ArrayList<>());
        drone.setModel(Model.Lighweight);
        drone.setSerial("Serial");
        drone.setState(State.IDLE);
        drone.setWeight(3);
        Optional<Double> actualCheckDroneBatteryLevelResult = this.droneActionsService.checkDroneBatteryLevel(drone);
        assertTrue(actualCheckDroneBatteryLevelResult.isPresent());
        assertEquals(10.0d, actualCheckDroneBatteryLevelResult.get());
        verify(drone).getBattery_capacity();
        verify(drone).setBattery_capacity((Double) any());
        verify(drone).setCurrent_carry_weight((Integer) any());
        verify(drone).setId((Long) any());
        verify(drone).setLoaded_medications((java.util.List<Medication>) any());
        verify(drone).setModel((Model) any());
        verify(drone).setSerial((String) any());
        verify(drone).setState((State) any());
        verify(drone).setWeight((Integer) any());
    }

    /**
     * Method under test: {@link DroneActionsService#getDrone(String)}
     */
    @Test
    void testGetDrone() {
        Drone drone = new Drone();
        drone.setBattery_capacity(10.0d);
        drone.setCurrent_carry_weight(3);
        drone.setId(123L);
        drone.setLoaded_medications(new ArrayList<>());
        drone.setModel(Model.Lighweight);
        drone.setSerial("Serial");
        drone.setState(State.IDLE);
        drone.setWeight(3);
        when(this.droneRepository.findOneByserial((String) any())).thenReturn(drone);
        assertTrue(this.droneActionsService.getDrone("Serial").isPresent());
        verify(this.droneRepository).findOneByserial((String) any());
    }

    /**
     * Method under test: {@link DroneActionsService#validateDrone(Drone)}
     */
    @Test
    void testValidateDrone() {
        Drone drone = new Drone();
        drone.setBattery_capacity(10.0d);
        drone.setCurrent_carry_weight(3);
        drone.setId(123L);
        drone.setLoaded_medications(new ArrayList<>());
        drone.setModel(Model.Lighweight);
        drone.setSerial("Serial");
        drone.setState(State.IDLE);
        drone.setWeight(3);
        Pair<Boolean, String> actualValidateDroneResult = this.droneActionsService.validateDrone(drone);
        assertFalse(actualValidateDroneResult.getKey());
        assertEquals("The provided drone battery capacity should be between 0 and 1", actualValidateDroneResult.getValue());
    }

    /**
     * Method under test: {@link DroneActionsService#validateDrone(Drone)}
     */
    @Test
    void testValidateDrone2() {
        Drone drone = mock(Drone.class);
        when(drone.getBattery_capacity()).thenReturn(10.0d);
        when(drone.getWeight()).thenReturn(3);
        doNothing().when(drone).setBattery_capacity((Double) any());
        doNothing().when(drone).setCurrent_carry_weight((Integer) any());
        doNothing().when(drone).setId((Long) any());
        doNothing().when(drone).setLoaded_medications((java.util.List<Medication>) any());
        doNothing().when(drone).setModel((Model) any());
        doNothing().when(drone).setSerial((String) any());
        doNothing().when(drone).setState((State) any());
        doNothing().when(drone).setWeight((Integer) any());
        drone.setBattery_capacity(10.0d);
        drone.setCurrent_carry_weight(3);
        drone.setId(123L);
        drone.setLoaded_medications(new ArrayList<>());
        drone.setModel(Model.Lighweight);
        drone.setSerial("Serial");
        drone.setState(State.IDLE);
        drone.setWeight(3);
        Pair<Boolean, String> actualValidateDroneResult = this.droneActionsService.validateDrone(drone);
        assertFalse(actualValidateDroneResult.getKey());
        assertEquals("The provided drone battery capacity should be between 0 and 1", actualValidateDroneResult.getValue());
        verify(drone).getBattery_capacity();
        verify(drone).getWeight();
        verify(drone).setBattery_capacity((Double) any());
        verify(drone).setCurrent_carry_weight((Integer) any());
        verify(drone).setId((Long) any());
        verify(drone).setLoaded_medications((java.util.List<Medication>) any());
        verify(drone).setModel((Model) any());
        verify(drone).setSerial((String) any());
        verify(drone).setState((State) any());
        verify(drone).setWeight((Integer) any());
    }

    /**
     * Method under test: {@link DroneActionsService#validateDrone(Drone)}
     */
    @Test
    void testValidateDrone3() {
        Drone drone = mock(Drone.class);
        when(drone.getBattery_capacity()).thenReturn(1.0d);
        when(drone.getWeight()).thenReturn(3);
        doNothing().when(drone).setBattery_capacity((Double) any());
        doNothing().when(drone).setCurrent_carry_weight((Integer) any());
        doNothing().when(drone).setId((Long) any());
        doNothing().when(drone).setLoaded_medications((java.util.List<Medication>) any());
        doNothing().when(drone).setModel((Model) any());
        doNothing().when(drone).setSerial((String) any());
        doNothing().when(drone).setState((State) any());
        doNothing().when(drone).setWeight((Integer) any());
        drone.setBattery_capacity(10.0d);
        drone.setCurrent_carry_weight(3);
        drone.setId(123L);
        drone.setLoaded_medications(new ArrayList<>());
        drone.setModel(Model.Lighweight);
        drone.setSerial("Serial");
        drone.setState(State.IDLE);
        drone.setWeight(3);
        Pair<Boolean, String> actualValidateDroneResult = this.droneActionsService.validateDrone(drone);
        assertTrue(actualValidateDroneResult.getKey());
        assertEquals("Operation successful", actualValidateDroneResult.getValue());
        verify(drone, atLeast(1)).getBattery_capacity();
        verify(drone).getWeight();
        verify(drone).setBattery_capacity((Double) any());
        verify(drone).setCurrent_carry_weight((Integer) any());
        verify(drone).setId((Long) any());
        verify(drone).setLoaded_medications((java.util.List<Medication>) any());
        verify(drone).setModel((Model) any());
        verify(drone).setSerial((String) any());
        verify(drone).setState((State) any());
        verify(drone).setWeight((Integer) any());
    }

    /**
     * Method under test: {@link DroneActionsService#validateDrone(Drone)}
     */
    @Test
    void testValidateDrone5() {
        Drone drone = mock(Drone.class);
        when(drone.getBattery_capacity()).thenReturn(-0.5d);
        when(drone.getWeight()).thenReturn(3);
        doNothing().when(drone).setBattery_capacity((Double) any());
        doNothing().when(drone).setCurrent_carry_weight((Integer) any());
        doNothing().when(drone).setId((Long) any());
        doNothing().when(drone).setLoaded_medications((java.util.List<Medication>) any());
        doNothing().when(drone).setModel((Model) any());
        doNothing().when(drone).setSerial((String) any());
        doNothing().when(drone).setState((State) any());
        doNothing().when(drone).setWeight((Integer) any());
        drone.setBattery_capacity(10.0d);
        drone.setCurrent_carry_weight(3);
        drone.setId(123L);
        drone.setLoaded_medications(new ArrayList<>());
        drone.setModel(Model.Lighweight);
        drone.setSerial("Serial");
        drone.setState(State.IDLE);
        drone.setWeight(3);
        Pair<Boolean, String> actualValidateDroneResult = this.droneActionsService.validateDrone(drone);
        assertFalse(actualValidateDroneResult.getKey());
        assertEquals("The provided drone battery capacity should be between 0 and 1", actualValidateDroneResult.getValue());
        verify(drone, atLeast(1)).getBattery_capacity();
        verify(drone).getWeight();
        verify(drone).setBattery_capacity((Double) any());
        verify(drone).setCurrent_carry_weight((Integer) any());
        verify(drone).setId((Long) any());
        verify(drone).setLoaded_medications((java.util.List<Medication>) any());
        verify(drone).setModel((Model) any());
        verify(drone).setSerial((String) any());
        verify(drone).setState((State) any());
        verify(drone).setWeight((Integer) any());
    }


    /**
     * Method under test: {@link DroneActionsService#validateMedication(Medication)}
     */
    @Test
    void testValidateMedication() {
        Medication medication = new Medication();
        medication.setCode("Code");
        medication.setDrone_id("Drone id");
        medication.setId(123L);
        medication.setImage("Image");
        medication.setName("Name");
        medication.setWeight(3);
        Pair<Boolean, String> actualValidateMedicationResult = this.droneActionsService.validateMedication(medication);
        assertFalse(actualValidateMedicationResult.getKey());
        assertEquals("Code only allows upper class letters,numbers,'-',''_", actualValidateMedicationResult.getValue());
    }

    /**
     * Method under test: {@link DroneActionsService#validateMedication(Medication)}
     */
    @Test
    void testValidateMedication2() {
        Medication medication = mock(Medication.class);
        when(medication.getCode()).thenReturn("Code");
        when(medication.getName()).thenReturn("Name");
        doNothing().when(medication).setCode((String) any());
        doNothing().when(medication).setDrone_id((String) any());
        doNothing().when(medication).setId((Long) any());
        doNothing().when(medication).setImage((String) any());
        doNothing().when(medication).setName((String) any());
        doNothing().when(medication).setWeight((Integer) any());
        medication.setCode("Code");
        medication.setDrone_id("Drone id");
        medication.setId(123L);
        medication.setImage("Image");
        medication.setName("Name");
        medication.setWeight(3);
        Pair<Boolean, String> actualValidateMedicationResult = this.droneActionsService.validateMedication(medication);
        assertFalse(actualValidateMedicationResult.getKey());
        assertEquals("Code only allows upper class letters,numbers,'-',''_", actualValidateMedicationResult.getValue());
        verify(medication).getCode();
        verify(medication).getName();
        verify(medication).setCode((String) any());
        verify(medication).setDrone_id((String) any());
        verify(medication).setId((Long) any());
        verify(medication).setImage((String) any());
        verify(medication).setName((String) any());
        verify(medication).setWeight((Integer) any());
    }

    /**
     * Method under test: {@link DroneActionsService#validateMedication(Medication)}
     */
    @Test
    void testValidateMedication3() {
        Medication medication = mock(Medication.class);
        when(medication.getCode()).thenReturn("[a-zA-Z0-9._-]");
        when(medication.getName()).thenReturn("Name");
        doNothing().when(medication).setCode((String) any());
        doNothing().when(medication).setDrone_id((String) any());
        doNothing().when(medication).setId((Long) any());
        doNothing().when(medication).setImage((String) any());
        doNothing().when(medication).setName((String) any());
        doNothing().when(medication).setWeight((Integer) any());
        medication.setCode("Code");
        medication.setDrone_id("Drone id");
        medication.setId(123L);
        medication.setImage("Image");
        medication.setName("Name");
        medication.setWeight(3);
        Pair<Boolean, String> actualValidateMedicationResult = this.droneActionsService.validateMedication(medication);
        assertTrue(actualValidateMedicationResult.getKey());
        assertEquals("Operation successful", actualValidateMedicationResult.getValue());
        verify(medication).getCode();
        verify(medication).getName();
        verify(medication).setCode((String) any());
        verify(medication).setDrone_id((String) any());
        verify(medication).setId((Long) any());
        verify(medication).setImage((String) any());
        verify(medication).setName((String) any());
        verify(medication).setWeight((Integer) any());
    }


    /**
     * Method under test: {@link DroneActionsService#validateMedication(Medication)}
     */
    @Test
    void testValidateMedication5() {
        Medication medication = mock(Medication.class);
        when(medication.getCode()).thenReturn("Code");
        when(medication.getName()).thenReturn("U");
        doNothing().when(medication).setCode((String) any());
        doNothing().when(medication).setDrone_id((String) any());
        doNothing().when(medication).setId((Long) any());
        doNothing().when(medication).setImage((String) any());
        doNothing().when(medication).setName((String) any());
        doNothing().when(medication).setWeight((Integer) any());
        medication.setCode("Code");
        medication.setDrone_id("Drone id");
        medication.setId(123L);
        medication.setImage("Image");
        medication.setName("Name");
        medication.setWeight(3);
        Pair<Boolean, String> actualValidateMedicationResult = this.droneActionsService.validateMedication(medication);
        assertFalse(actualValidateMedicationResult.getKey());
        assertEquals("Name only allows letters,numbers,'-',''_", actualValidateMedicationResult.getValue());
        verify(medication).getName();
        verify(medication).setCode((String) any());
        verify(medication).setDrone_id((String) any());
        verify(medication).setId((Long) any());
        verify(medication).setImage((String) any());
        verify(medication).setName((String) any());
        verify(medication).setWeight((Integer) any());
    }


    /**
     * Method under test: {@link DroneActionsService#performRegex(String, String)}
     */
    @Test
    void testPerformRegex() {
        assertEquals(0, this.droneActionsService.performRegex("Reg", "Str2 Check").intValue());
        assertEquals(3, this.droneActionsService.performRegex("[A-Z0-9._-]", "Str2 Check").intValue());
        assertEquals(0, this.droneActionsService.performRegex("", "Str2 Check").intValue());
    }

    /**
     * Method under test: {@link DroneActionsService#canLoadDroneWithMedication(Drone, Medication)}
     */
    @Test
    void testCanLoadDroneWithMedication() {
        Drone drone = new Drone();
        drone.setBattery_capacity(10.0d);
        drone.setCurrent_carry_weight(3);
        drone.setId(123L);
        drone.setLoaded_medications(new ArrayList<>());
        drone.setModel(Model.Lighweight);
        drone.setSerial("Serial");
        drone.setState(State.IDLE);
        drone.setWeight(3);

        Medication medication = new Medication();
        medication.setCode("Code");
        medication.setDrone_id("Drone id");
        medication.setId(123L);
        medication.setImage("Image");
        medication.setName("Name");
        medication.setWeight(3);
        Pair<Boolean, String> actualCanLoadDroneWithMedicationResult = this.droneActionsService
                .canLoadDroneWithMedication(drone, medication);
        assertTrue(actualCanLoadDroneWithMedicationResult.getKey());
        assertEquals("Validation successful!", actualCanLoadDroneWithMedicationResult.getValue());
    }

    /**
     * Method under test: {@link DroneActionsService#canLoadDroneWithMedication(Drone, Medication)}
     */
    @Test
    void testCanLoadDroneWithMedication2() {
        Drone drone = mock(Drone.class);
        when(drone.getBattery_capacity()).thenReturn(10.0d);
        when(drone.getLoaded_medications()).thenReturn(new ArrayList<>());
        doNothing().when(drone).setBattery_capacity((Double) any());
        doNothing().when(drone).setCurrent_carry_weight((Integer) any());
        doNothing().when(drone).setId((Long) any());
        doNothing().when(drone).setLoaded_medications((List<Medication>) any());
        doNothing().when(drone).setModel((Model) any());
        doNothing().when(drone).setSerial((String) any());
        doNothing().when(drone).setState((State) any());
        doNothing().when(drone).setWeight((Integer) any());
        drone.setBattery_capacity(10.0d);
        drone.setCurrent_carry_weight(3);
        drone.setId(123L);
        drone.setLoaded_medications(new ArrayList<>());
        drone.setModel(Model.Lighweight);
        drone.setSerial("Serial");
        drone.setState(State.IDLE);
        drone.setWeight(3);

        Medication medication = new Medication();
        medication.setCode("Code");
        medication.setDrone_id("Drone id");
        medication.setId(123L);
        medication.setImage("Image");
        medication.setName("Name");
        medication.setWeight(3);
        Pair<Boolean, String> actualCanLoadDroneWithMedicationResult = this.droneActionsService
                .canLoadDroneWithMedication(drone, medication);
        assertTrue(actualCanLoadDroneWithMedicationResult.getKey());
        assertEquals("Validation successful!", actualCanLoadDroneWithMedicationResult.getValue());
        verify(drone).getBattery_capacity();
        verify(drone).getLoaded_medications();
        verify(drone).setBattery_capacity((Double) any());
        verify(drone).setCurrent_carry_weight((Integer) any());
        verify(drone).setId((Long) any());
        verify(drone).setLoaded_medications((List<Medication>) any());
        verify(drone).setModel((Model) any());
        verify(drone).setSerial((String) any());
        verify(drone).setState((State) any());
        verify(drone).setWeight((Integer) any());
    }

    /**
     * Method under test: {@link DroneActionsService#canLoadDroneWithMedication(Drone, Medication)}
     */
    @Test
    void testCanLoadDroneWithMedication3() {
        Drone drone = mock(Drone.class);
        when(drone.getBattery_capacity()).thenReturn(-0.5d);
        when(drone.getLoaded_medications()).thenReturn(new ArrayList<>());
        doNothing().when(drone).setBattery_capacity((Double) any());
        doNothing().when(drone).setCurrent_carry_weight((Integer) any());
        doNothing().when(drone).setId((Long) any());
        doNothing().when(drone).setLoaded_medications((List<Medication>) any());
        doNothing().when(drone).setModel((Model) any());
        doNothing().when(drone).setSerial((String) any());
        doNothing().when(drone).setState((State) any());
        doNothing().when(drone).setWeight((Integer) any());
        drone.setBattery_capacity(10.0d);
        drone.setCurrent_carry_weight(3);
        drone.setId(123L);
        drone.setLoaded_medications(new ArrayList<>());
        drone.setModel(Model.Lighweight);
        drone.setSerial("Serial");
        drone.setState(State.IDLE);
        drone.setWeight(3);

        Medication medication = new Medication();
        medication.setCode("Code");
        medication.setDrone_id("Drone id");
        medication.setId(123L);
        medication.setImage("Image");
        medication.setName("Name");
        medication.setWeight(3);
        Pair<Boolean, String> actualCanLoadDroneWithMedicationResult = this.droneActionsService
                .canLoadDroneWithMedication(drone, medication);
        assertFalse(actualCanLoadDroneWithMedicationResult.getKey());
        assertEquals("Drone battery Capacity is low!", actualCanLoadDroneWithMedicationResult.getValue());
        verify(drone).getBattery_capacity();
        verify(drone).getLoaded_medications();
        verify(drone).setBattery_capacity((Double) any());
        verify(drone).setCurrent_carry_weight((Integer) any());
        verify(drone).setId((Long) any());
        verify(drone).setLoaded_medications((List<Medication>) any());
        verify(drone).setModel((Model) any());
        verify(drone).setSerial((String) any());
        verify(drone).setState((State) any());
        verify(drone).setWeight((Integer) any());
    }

    /**
     * Method under test: {@link DroneActionsService#listAllDrones()}
     */
    @Test
    void testListAllDrones() {
        when(this.droneRepository.findAll()).thenReturn(new ArrayList<>());
        assertTrue(this.droneActionsService.listAllDrones().isPresent());
        verify(this.droneRepository).findAll();
    }
}

