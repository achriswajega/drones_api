package com.musala.droneAPi.dao;

import com.musala.droneAPi.pojos.Medication;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicationRepository extends JpaRepository<Medication,Integer> {
}
