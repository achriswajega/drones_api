package com.musala.droneAPi.dao;

import com.musala.droneAPi.pojos.Drone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface DroneRepository extends JpaRepository<Drone,Integer> {
    public Drone findOneByserial(String serial);
}
