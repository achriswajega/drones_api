package com.musala.droneAPi.services;

import com.musala.droneAPi.pojos.Drone;
import com.musala.droneAPi.pojos.Medication;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;


import java.util.List;
import java.util.Optional;

public interface DroneActionsInterface {
    public Pair<Boolean, String> saveNewDrone(Drone drone);

    public Pair<Boolean, String> loadMedicationToDrone(Drone drone, Medication medication);

    public Triple<Boolean,Optional<List<Medication>>,String> getLoadedMedicationsOnDrone(Drone drone);

    public Optional<List<Drone>> getAvalableDronesForLoading();

    public Optional<Double> checkDroneBatteryLevel(Drone drone);

    public Optional<Drone> getDrone(String serial_number);

    public Pair<Boolean, String> validateDrone(Drone drone);

    public Pair<Boolean, String> validateMedication(Medication medication);

    public Integer performRegex(String reg,String str2Check);

    //-----> Functional Requirements
    public Pair<Boolean, String> canLoadDroneWithMedication(Drone drone, Medication medication);

    public Optional<List<Drone>> listAllDrones();

}
