package com.musala.droneAPi.services;

import com.musala.droneAPi.dao.DroneRepository;
import com.musala.droneAPi.pojos.Drone;
import com.musala.droneAPi.pojos.Medication;


import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class DroneActionsService implements DroneActionsInterface {
    @Autowired
    private DroneRepository droneRepository;
    private static final Logger logger = LogManager.getLogger(DroneActionsService.class);

    public DroneActionsService(DroneRepository droneRepository) {
        this.droneRepository = droneRepository;
    }

    @Override
    public Pair<Boolean, String> saveNewDrone(Drone drone) {
        logger.info("DroneActionApi.new.request adding new drone {} | {}", drone.getSerial(), drone.getModel());
        try {
            Pair<Boolean, String> validateDrone = validateDrone(drone);
            if (validateDrone.getLeft()) {
                //-------> Drone Record is valid
                droneRepository.save(drone);
                logger.info("DroneActionApi.new.request successfully added new drone {} | {}", drone.getSerial(), drone.getModel());
                return Pair.of(true, validateDrone.getRight());
            } else {
                //----------> Drone Record is not valid
                logger.error("DroneActionApi.new.request failed to add new drone error: {} | {} | {}", validateDrone.getRight(), drone.getSerial(), drone.getModel());
                return Pair.of(false, validateDrone.getRight());
            }

        } catch (Exception e) {
            logger.error("DroneActionApi.new.request error occurred while adding new drone error:{} | {} | {} ", e.getMessage(), drone.getSerial(), drone.getModel());
            return Pair.of(false, "An internal error occurred");
        }

    }

    @Override
    public Pair<Boolean, String> loadMedicationToDrone(Drone drone, Medication medication) {
        logger.info("DroneActionApi.new.request adding medication to drone {} | {} | {}", drone.getSerial(), drone.getModel(), medication.getName());

        Pair<Boolean, String> validateMedication = validateMedication(medication);
        if (!validateMedication.getLeft()) {
            logger.info("DroneActionApi.new.request failed to add medication to drone error: {} | {} | {} | {}", validateMedication.getRight(), drone.getSerial(), drone.getModel(), medication.getName());
            return Pair.of(false, validateMedication.getRight());
        }

        //------> Check the current state Medicine Weight --------


        try
        {

            //----> drone loading rules
            Pair<Boolean, String> loadingRulesResponse = canLoadDroneWithMedication(drone,medication);
            if (!loadingRulesResponse.getLeft()){
                logger.info("DroneActionApi.new.request failed to add medication to drone error: {} | {} | {} | {}", "Exceeds maximum allowed carry weight", drone.getSerial(), drone.getModel(), medication.getName());
                return Pair.of(false, loadingRulesResponse.getRight());
            }


            drone.getLoaded_medications().add(medication);
            drone.setCurrent_carry_weight(drone.getCurrent_carry_weight() + medication.getWeight());
            droneRepository.saveAndFlush(drone);
            return Pair.of(true, validateMedication.getRight());

        } catch (Exception e) {
            logger.error("DroneActionApi.new.request error occurred while adding medication to drone error:{} | {} | {} ", e.getMessage(), drone.getSerial(), drone.getModel());
            return Pair.of(false, "An internal error occurred");
        }

    }

    @Override
    public Triple<Boolean, Optional<List<Medication>>, String> getLoadedMedicationsOnDrone(Drone drone) {
        logger.info("DroneActionApi.new.request get medication loaded on drone {} | {} ", drone.getSerial(), drone.getModel());
        Pair<Boolean, String> validateDrone = validateDrone(drone);
        if (!validateDrone.getLeft()) {
            logger.error("DroneActionApi.new.request failed to add new drone error: {} | {} | {}", validateDrone.getRight(), drone.getSerial(), drone.getModel());
            return Triple.of(false, null, validateDrone.getRight());
        }
        return Triple.of(false, Optional.ofNullable(drone.getLoaded_medications()), validateDrone.getRight());
    }

    @Override
    public Optional<List<Drone>> getAvalableDronesForLoading() {
        return Optional.of(droneRepository.findAll().stream().filter(drone -> drone.getCurrent_carry_weight()< 500).filter(drone -> drone.getBattery_capacity() > 0.25).collect(Collectors.toList()));
    }

    @Override
    public Optional<Double> checkDroneBatteryLevel(Drone drone) {
        return Optional.ofNullable(drone.getBattery_capacity());
    }

    @Override
    public Optional<Drone> getDrone(String serial) {
        return Optional.ofNullable(droneRepository.findOneByserial(serial));
    }

    @Override
    public Pair<Boolean, String> validateDrone(Drone drone) {
        if (drone == null) return Pair.of(false, "You can't register a null drone!");
        if (drone.getWeight() > 500) return Pair.of(false, "The provided drone weight exceeds the maximum of 500gr");
        if (drone.getBattery_capacity() > 1 || drone.getBattery_capacity() < 0)
            return Pair.of(false, "The provided drone battery capacity should be between 0 and 1");
        return Pair.of(true, "Operation successful");
    }

    @Override
    public Pair<Boolean, String> validateMedication(Medication medication) {

        if (medication == null) return Pair.of(false, "You can't register null medication!");
        //----name allowed
        if (performRegex("[a-zA-Z0-9._-]", medication.getName()) < 2)
            return Pair.of(false, "Name only allows letters,numbers,'-',''_");
        if (performRegex("[A-Z0-9._-]", medication.getCode()) < 2)
            return Pair.of(false, "Code only allows upper class letters,numbers,'-',''_");
        return Pair.of(true, "Operation successful");
    }

    @Override
    public Integer performRegex(String reg, String str2Check) {
        List<String> result = new ArrayList<String>();
        Pattern pattern = Pattern.compile(reg);
        Matcher matcher = pattern.matcher(str2Check);

        while (matcher.find()) {
            if (matcher.group().length() != 0) {
                result.add(matcher.group().trim());
            }
        }


        return result.size();
    }

    @Override
    public Pair<Boolean, String> canLoadDroneWithMedication(Drone drone, Medication medication) {

        //---> get the current medicatication weight
        Integer medicationWeight = medication.getWeight();
        Integer currentDroneWeight = drone.getLoaded_medications().stream().map(Medication::getWeight).reduce(0, Integer::sum);
        //-----> check the max weight capacity
        if ((medicationWeight + currentDroneWeight) > 500) return Pair.of(false,"Maximum acceptable weight exceeded!");

        //-----> Check the battery capacity threshold
        if (drone.getBattery_capacity() < 0.25) return Pair.of(false,"Drone battery Capacity is low!");

        return Pair.of(true,"Validation successful!");
    }

    @Override
    public Optional<List<Drone>> listAllDrones() {
        return Optional.of(droneRepository.findAll());
    }
}
