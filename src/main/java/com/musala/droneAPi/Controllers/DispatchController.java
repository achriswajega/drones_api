package com.musala.droneAPi.Controllers;

import com.musala.droneAPi.Requests.LoadDroneWithMedicationRequest;
import com.musala.droneAPi.Requests.RegisterNewDroneRequest;
import com.musala.droneAPi.Responses.ApiResponse;
import com.musala.droneAPi.pojos.Drone;
import com.musala.droneAPi.pojos.Medication;
import com.musala.droneAPi.services.DroneActionsService;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1")
public class DispatchController {
    private static final Logger logger = LogManager.getLogger(DroneActionsService.class);
    @Autowired
    private DroneActionsService droneActionsService;



    //-----------> Register New Drone ---------------
    @PostMapping(path = "/drones/register",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ApiResponse<String>> registerNewDrones(@RequestBody RegisterNewDroneRequest request){
        ApiResponse<String> response = new ApiResponse<>();
        if (request == null){
            response.setResponseCode(400);
            response.setStatus("Failed");
            response.setData("Operation Failed");
            response.setDescription("Bad Request please provide a valid body");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }

        logger.info("new register drone request");

        Drone drone = new Drone();
        drone.setBattery_capacity(request.getBattery_capacity());
        drone.setModel(request.getModel());
        drone.setWeight(request.getWeight());
        drone.setState(request.getState());
        drone.setSerial(request.getSerial());

        Pair<Boolean,String> saveNewDroneResponse = droneActionsService.saveNewDrone(drone);
        if (saveNewDroneResponse.getLeft()){
            response.setResponseCode(200);
            response.setStatus("Success");
            response.setData("Operation successful");
            response.setDescription(saveNewDroneResponse.getRight());
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(response);
        }
        response.setResponseCode(400);
        response.setStatus("Failed");
        response.setData("Operation Failed");
        response.setDescription(saveNewDroneResponse.getRight());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);

    }


    //-----------------------> Load Drone with Medication --------------------
    @PostMapping(path = "/drones/loadMedication",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ApiResponse<String>> loadMedicationToDrone(@RequestBody LoadDroneWithMedicationRequest request){

        ApiResponse<String> response = new ApiResponse<>();
        Optional<Drone> drone = droneActionsService.getDrone(request.getSerial_number());

        if (!drone.isPresent()){
            //--------> Invalid Drone Serial Number has been provided
            response.setResponseCode(400);
            response.setStatus("Failed");
            response.setDescription("Please provide a valid drone serial Number");
            response.setData("Operation Failed");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }

        //---> getting medication to be loaded to Drone
        Medication medication = request.getMedication();

        Pair<Boolean,String> loadDroneWithMedicationResponse = droneActionsService.loadMedicationToDrone(drone.get(),medication);
        if (loadDroneWithMedicationResponse.getLeft()){
            response.setResponseCode(200);
            response.setStatus("Success");
            response.setData("Operation successful");
            response.setDescription(loadDroneWithMedicationResponse.getRight());
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(response);
        }
        response.setResponseCode(400);
        response.setStatus("Failed");
        response.setData("Operation Failed");
        response.setDescription(loadDroneWithMedicationResponse.getRight());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);


    }



    //--------> Get Drones Available for Loading
    @GetMapping(path = "/drones/list/availableForLoading",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ApiResponse<Optional<List<Drone>>>> listAllDronesAvailableForLoading(){
        ApiResponse<Optional<List<Drone>>> response = new ApiResponse<>();
        response.setResponseCode(200);
        response.setStatus("Success");
        response.setData(droneActionsService.getAvalableDronesForLoading());
        response.setDescription("Operation successful!!");
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(response);
    }


    @GetMapping(path = "/drones/find/battreyLevel/{serial_number}",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ApiResponse<HashMap<String,Object>>> getDroneBattreyLevel(@PathVariable String serial_number){
        HashMap<String,Object> response_data = new HashMap<>();


       Optional<Drone> drone = droneActionsService.getDrone(serial_number);
        ApiResponse<HashMap<String,Object>> response = new ApiResponse<>();
       if (!drone.isPresent()){

           response.setResponseCode(500);
           response.setStatus("Failed");
           response.setData(response_data);
           response.setDescription("Please provided a valid Drone Serial Number!");
           return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
       }


        response_data.put("Serial Number",drone.get().getSerial());
        response_data.put("Battery Level",drone.get().getBattery_capacity());

        response.setResponseCode(200);
        response.setStatus("Success");
        response.setData(response_data);
        response.setDescription("Operation successful!!");
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(response);
    }



    @GetMapping(path = "/drones/listAll",produces = MediaType.APPLICATION_JSON_VALUE)
    public Optional<List<Drone>> listAllDrones(){
        return droneActionsService.listAllDrones();
    }


    @GetMapping(path = "/drones/find/{serial_number}",produces = MediaType.APPLICATION_JSON_VALUE)
    public Optional<Drone> getDroneBySerialNumber(@PathVariable String serial_number){
        return droneActionsService.getDrone(serial_number);
    }



}
