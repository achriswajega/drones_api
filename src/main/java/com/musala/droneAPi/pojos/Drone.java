package com.musala.droneAPi.pojos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Drone {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    private String serial;
    private Model model;
    private Double battery_capacity;
    private Integer weight;
    private Integer current_carry_weight = 0;
    private State state;
    @OneToMany(cascade = {CascadeType.ALL})
    @JoinColumn(name = "drone_id")
    private List<Medication> loaded_medications;


}


