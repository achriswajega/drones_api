package com.musala.droneAPi.pojos;

public enum Model {
    Lighweight, Middleweight, Cruiseweight, Headweight
}
