package com.musala.droneAPi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DroneAPiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DroneAPiApplication.class, args);
	}

}
