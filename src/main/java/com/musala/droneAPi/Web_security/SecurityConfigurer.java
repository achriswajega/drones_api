package com.musala.droneAPi.Web_security;


import com.musala.droneAPi.Web_security.utils.JwtRequestFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableWebSecurity
public class SecurityConfigurer extends WebSecurityConfigurerAdapter {

    @Autowired
    private MyUserDetailsService myUserDetailsService;



    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception{
        auth.userDetailsService(myUserDetailsService);
    }


    @Bean
    public PasswordEncoder passwordEncoder(){
        return NoOpPasswordEncoder.getInstance();
    }

    @Autowired
    private JwtRequestFilter jwtRequestFilter;




    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()

                //-----------> Don't Authenticate these endpoints
                .antMatchers("/auth/**").permitAll()


                //-----> Authenticate All Requests
                .anyRequest().authenticated()

                //------> Let Spring Security Not Manage State
                .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        //---------> Lets implement the jwt authentication
        http.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);


        http.csrf().ignoringAntMatchers("/*/**");//.disable();
        http.cors().and().csrf().disable();



    }




    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }



}
