package com.musala.droneAPi.Requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.musala.droneAPi.pojos.Medication;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoadDroneWithMedicationRequest {
    @JsonProperty("serial_number")
    private String serial_number;
    @JsonProperty("medication")
    private Medication medication;

}
