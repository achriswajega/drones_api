package com.musala.droneAPi.Requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.musala.droneAPi.pojos.Model;
import com.musala.droneAPi.pojos.State;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class RegisterNewDroneRequest {
    @JsonProperty("serial")
    private String serial;
    @JsonProperty("model")
    private Model model;
    @JsonProperty("battery_capacity")
    private Double battery_capacity;
    @JsonProperty("weight")
    private Integer weight;
    @JsonProperty("state")
    private State state;
}
