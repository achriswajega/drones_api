## Drones API

#### Backend for MASALA SOFTWARE Technical Interview
Keywords: `Java backend`, `H2-Db`, `REST API`,`SpringBoot`, `Maven`



### 1. Descriptions<br/>
This is an Java implementation of the backend for Masala Soft Technical Interview.
* The backend is running on port 8080.
* SpringBoot
* H2 DB
* Username : test
* Password : test123

------


### 2. Project/IDE Setup
* Command Line:
    * Install [maven](https://maven.apache.org/index.html)
    * To compile your code simply run mvn compile in the root directory (the folder
      that has pom.xml)
    * To run your code run mvn exec:java
* Eclipse:
    * File → Import → Maven → Existing Maven Projects
    * Navigate to the project location
    * Right click the project → Maven Build…
    * Input compile exec:java in the Goals input box
    * Click apply then run/close
    * You can now run the project by pressing the green play button
* Intellij:
    * Import project
    * Navigate to the project location
    * Import project from external model → Maven
    * Next → Next → Next → Finish
    * Add Configuration → + Button → Maven
    * Name the configuration and input `exec:java` in the Command Line box
    * Apply → Ok
    * You can now run the project by pressing the green play button

------

### 3. Testing
* Response Body:
    * JSON format

* Response Status:
    * 200 OK for a successful add
    * 400 BAD REQUEST if the request body is improperly formatted.
----

### 4. Existing Endpoints
* (Post)  http://localhost:8080/auth/authenticate
    * Request Body
      `{
      "username":"test",
      "password":"test123"
      }`<br/>
    * Response Body `{
      "jwt": "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0IiwiZXhwIjoxNjU0ODY0OTAyLCJpYXQiOjE2NTQ4NjQzMDJ9.1QS2uCskxAwo56Ni-Su7k_6k9UnRABN9pBveWHq7cEg"
      }` <br />
    * Curl Request `curl --location --request POST 'http://localhost:8080/auth/authenticate' \
      --header 'Content-Type: application/json' \
      --header 'Cookie: JSESSIONID=75FD51F8D1F4523B431158CC8561D3F3' \
      --data-raw '{
      "username":"test",
      "password":"test123"
      }'` <br />


### Registering New Drone
* (Post) http://localhost:8080/api/v1/drones/register
   
    * Curl Request `curl --location --request POST 'http://localhost:8080/api/v1/drones/register' \
      --header 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0IiwiZXhwIjoxNjU0ODY0OTAyLCJpYXQiOjE2NTQ4NjQzMDJ9.1QS2uCskxAwo56Ni-Su7k_6k9UnRABN9pBveWHq7cEg' \
      --header 'Content-Type: application/json' \
      --data-raw '{
      "serial": "25522",
      "model": "Lighweight",
      "battery_capacity": 0.29,
      "weight": 12,
      "state": "IDLE"
      }'`<br />
    * 
---
### Load Drone With Medication
* (Post)  http://localhost:8080/api/v1/drones/loadMedication

    * Curl Request `curl --location --request POST 'http://localhost:8080/api/v1/drones/loadMedication' \
      --header 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0IiwiZXhwIjoxNjU0ODY0OTAyLCJpYXQiOjE2NTQ4NjQzMDJ9.1QS2uCskxAwo56Ni-Su7k_6k9UnRABN9pBveWHq7cEg' \
      --header 'Content-Type: application/json' \
      --data-raw '{
      "serial_number": "25522",
      "medication": {
      "name": "Panadol",
      "weight": 120,
      "code": "HDYD",
      "image": "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.haleon.com%2Four-brands%2Fpanadol%2F&psig=AOvVaw26aPz0WpvgZzu3KAh7ulST&ust=1669227003515000&source=images&cd=vfe&ved=0CBAQjRxqFwoTCLCOz6axwvsCFQAAAAAdAAAAABAJ"
      }
      }'`

---


### Drones Available for Loading
* (Get)  http://localhost:8080/api/v1/drones/list/availableForLoading

    * Curl Request `curl --location --request GET 'http://localhost:8080/api/v1/drones/list/availableForLoading' \
      --header 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0IiwiZXhwIjoxNjU0ODY0OTAyLCJpYXQiOjE2NTQ4NjQzMDJ9.1QS2uCskxAwo56Ni-Su7k_6k9UnRABN9pBveWHq7cEg'`

---


### Drone Battery Level
* (Get)  http://localhost:8080/api/v1/drones/find/battreyLevel/25522

    * Curl Request `curl --location --request GET 'http://localhost:8080/api/v1/drones/find/battreyLevel/25522' \
      --header 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0IiwiZXhwIjoxNjU0ODY0OTAyLCJpYXQiOjE2NTQ4NjQzMDJ9.1QS2uCskxAwo56Ni-Su7k_6k9UnRABN9pBveWHq7cEg'`

---
